{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2612 Advanced Programming for Data Science"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Quick guide to Course Structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. [Object oriented programming](01-ObjectOrientedProgramming.ipynb)\n",
    "1. [Documentation and Testing](02-DocumentationAndTesting.ipynb)\n",
    "1. [Version Control with git](03-VersionControl.ipynb)\n",
    "1. [Software Deployment](04-SoftwareDeployment.ipynb)\n",
    "1. [Time-series Analysis](05-TimeseriesAnalysis.ipynb)\n",
    "1. [Scalable computing: Big data, Dask, ML in big data](06-Scale.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-danger\">\n",
    "    <b>This course assumes you have knowledge of python at a basic level.</b>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Advanced Programming encompasses concepts and procedures beyond what you may learn in introductory tutorials. The goal of the course is to show you how a software project would be handled in a corporate environment by creating a robust workflow. The latter means a collection of trustworthy methods by which you build, piece-by-piece, a reusable, reproducible, and reliable set of software tools."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why python?\n",
    "\n",
    "The Python language has demonstrated to be a powerful tool. It is user-friendly and widespread. You will probably have already faced the question:\n",
    "\n",
    "[__Is python the language for this project?__](https://www.ishir.com/blog/36749/top-75-programming-languages-in-2021-comparison-and-by-type.htm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, as we will see, it does not mean the python language is a silver bullet against every coding challenge. Python is also in constant evolution. As we will see, python has many [__Enhancement Proposals__](https://www.python.org/dev/peps/), or __PEP__.\n",
    "\n",
    "The major weakness of python is its *scalability*. We will adress that later on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Why Jupyter?\n",
    "\n",
    "You are free to use whatever IDE you feel most comfortable with. In the demos used in the classes, jupyter-lab is used. It is mostly the same platform across every OS. [__Jupyter-lab__](https://jupyter.org/) also offers just a minimal text edition support, so we can develop coding skills without \"training wheels\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### As per the Syllabus "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this unit, students get acquainted with advanced concepts in programming. More complex\n",
    "concepts allow for higher level abstractions in code. Code abstractions like objects and classes\n",
    "allow increased functionality, faster deployment, and enable collaborations in software projects.\n",
    "\n",
    "The __life cycle management__ of a data science product in a corporate environment must follow\n",
    "specific guidelines. The guidelines, while built on top of common programming frameworks,\n",
    "differ in the specificities of the mathematical models used. Present-day approaches to\n",
    "programming and project management are introduced and explored."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Modules"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Introduction to advanced programming concepts. Concepts for Code reusability, style guides, and linting. Learn by doing: example of time series analysis with pandas.\n",
    "2. Documentation and Testing. Unit tests, Functional tests, and Integration tests with the pytest framework.\n",
    "3. Version control with git. Automating linting and testing with Continuous Integration/Continuous Delivery.\n",
    "4. Software deployment: virtual environments with conda and dockers.\n",
    "5. Time-Series: analysis, decomposition, and forecasting.\n",
    "6. Working at scale: machine learning in Big Data with Dask.\n",
    "\n",
    "It may be possible some delay in the classes might ocurr due to technical dificulties. If that is the case, the content of one module will continue into the next, at a brisker pace. Modules 3 and 5 are also designed to be used as buffer classes, in case of major setbacks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "### The skillset you will acquire"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Reliability\n",
    "    * Write legible and structured code.\n",
    "    * Document code and projects.\n",
    "* Resilience\n",
    "    * Learn coordination, collaboration, and code versioning best practices.\n",
    "    * Develop tests for code quality assurance.\n",
    "    * Create and manage virtual environments in python.\n",
    "* Durability\n",
    "    * Learn the fundamentals of code versioning control.\n",
    "    * Automate linting and testing\n",
    "* Scale\n",
    "    * Understand concepts of Big Data\n",
    "    * Learn the fundamentals of ML at large scales\n",
    "    \n",
    "But most important: be critical of your own way of thinking via software development.\n",
    "\n",
    "__*And understand the huge ammount of pain that is developing software.*__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The overall evaluation of performance consists of 3 parts, by University Standards:\n",
    "    \n",
    "* Class participation through 3 quizzes (20%)\n",
    "    * At the end of weeks 3, 4, and 6.\n",
    "    * Best 2 out of 3 grades are accounted for.\n",
    "* Group project (30%)\n",
    "    * First part presented at the end of week 2.\n",
    "    * Second part presented at the end of week 4.\n",
    "* Final exam (50%)\n",
    "    * After all classes.\n",
    "\n",
    "Students need to participate in class quizzes for at least 2 times. Best 2 out of 3.\n",
    "\n",
    "You will develop a coding project using a public dataset and create a data analysis\n",
    "pipeline that must pass a series of tests. The tests will be executed in the pytest framework. The code of the project will be stored in a repository together with instructions on how to generate an appropriate virtual environment. Anyone with the instructions should be able to clone the repository, install the virtual environment, and run the analysis developed by the students."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bibliography"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* [The replication crisis](https://en.wikipedia.org/wiki/Replication_crisis)\n",
    "* [Data Lifecycle Management](https://medium.com/jagoanhosting/what-is-data-lifecycle-management-and-what-phases-would-it-pass-through-94dbd207ff54)\n",
    "* The contents of these notebooks\n",
    "* [Pep8](https://www.python.org/dev/peps/pep-0008/)\n",
    "* [Flake8](https://flake8.pycqa.org/en/latest/)\n",
    "* [Sphinx](https://www.sphinx-doc.org/en/master/)\n",
    "* [git](https://git-scm.com/)\n",
    "* [pytest](https://docs.pytest.org/en/stable/)\n",
    "* [Pip](https://pip.pypa.io/en/stable/)\n",
    "* [Conda](https://docs.conda.io/en/latest/)\n",
    "* [Virtualenv](https://virtualenv.pypa.io/en/latest/)\n",
    "* Time series analysis\n",
    "* [Dask](https://dask.org/)\n",
    "* Last year we tackled [Apache Spark](https://spark.apache.org/). The notes are still on the repository, if you feel curious."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
